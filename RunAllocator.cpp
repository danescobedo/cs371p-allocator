// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    /*
    your code for the real eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    */
    
    
    int testcases;
    cin >> testcases;
    cin.ignore();
    for(int i = 0; i < testcases; ++i){
        int storage = 1000;
        vector<int> sentinels;
        sentinels.push_back(0); storage -= 8;
        sentinels.push_back(984); storage -= 8;
        
        vector<int> data;
        char currNum;
        cin.getline(char_type *__s, streamsize __n) >> currNum;
        cin.ignore();
        do{
            data.push_back(currNum);
        }while(currNum != '\n');
    return 0;}
